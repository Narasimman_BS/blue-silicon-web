$(document).ready(function () {

    $('#site-header-wrap').load('header.html');
    $('#clients').load('clients.html');
    $('#footer').load('footer.html');
    $('#bottom').load('copyrights.html');

});


var frontlayer = $('.frontlayer');
var bglayer = $('.bglayer');
var overlay = $('#landing-cover');
var mock2 = $('.mock-2');
var mock3 = $('.mock-3');

// Move front layer a bit more than the bg layer.
frontlayer.animate({
    textIndent: 0
}, {
    step: function (now, fx) {
        overlay.mousemove(function (e) {
            var amountMovedX = (e.pageX * -1 / 5);
            var amountMovedY = (e.pageY * -1 / 12);
            frontlayer.css({
                '-webkit-transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)',
                '-moz-transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)',
                '-ms-transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)',
                '-o-transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)',
                'transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)'
            });
            // mock3.css({
            //     '-webkit-transform': 'translate(' + amountMovedY + 'px, 0)',
            //     '-moz-transform': 'translate(' + amountMovedY + 'px, )',
            //     '-ms-transform': 'translate(' + amountMovedY + 'px, 0)',
            //     '-o-transform': 'translate(' + amountMovedY + 'px, 0)',
            //     'transform': 'translate(' + amountMovedY + 'px, 0)'
            // });
        });
    },
    duration: 1000
}, 'easeOutCubic');


//bg animate layer a bit slower also seen on amountMovedX.

bglayer.animate({
    textIndent: 0
}, {
    step: function (now, fx) {
        overlay.mousemove(function (e) {
            var amountMovedX = (e.pageX * -1 / 10);
            var amountMovedY = (e.pageY * -1 / 15);
            bglayer.css({
                '-webkit-transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)',
                '-moz-transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)',
                '-ms-transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)',
                '-o-transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)',
                'transform': 'translate3d(' + amountMovedX + 'px,' + amountMovedY + 'px, 0)'
            });

            // mock2.css({
            //     '-webkit-transform': 'translate(' + amountMovedY + 'px, 0)',
            //     '-moz-transform': 'translate(' + amountMovedY + 'px, )',
            //     '-ms-transform': 'translate(' + amountMovedY + 'px, 0)',
            //     '-o-transform': 'translate(' + amountMovedY + 'px, 0)',
            //     'transform': 'translate(' + amountMovedY + 'px, 0)'
            // });
        });
    },
    duration: 5000
}, 'easeOutCubic');

// Move front layer a bit more than the bg layer.

mock3.animate({
    textIndent: 0
}, {
    step: function (now, fx) {
        overlay.mousemove(function (e) {
            // var amountMovedX = (e.pageX * -1 / 5);
            // var amountMovedY = (e.pageY * -0.5 / 4 );
            var amountMovedY = (e.pageY * -1 / 5);
            // alert(amountMovedY);
            // mock3.css({
            //     'top:' + amountMovedY + 'px'
            // });
            if (amountMovedY <= (-35)) {
                mock3.css({
                    '-webkit-transform': 'translate(' + amountMovedY + 'px, 1px)',
                    '-moz-transform': 'translate(' + amountMovedY + 'px, 1px)',
                    '-ms-transform': 'translate(' + amountMovedY + 'px, 1px)',
                    '-o-transform': 'translate(' + amountMovedY + 'px, 1px)',
                    'transform': 'translate(' + amountMovedY + 'px, 1px)'
                });
            } else {

                mock3.css({
                    '-webkit-transform': 'translate(122px, -2px)',
                    '-moz-transform': 'translate(122px, -2px)',
                    '-ms-transform': 'translate(122px, -2px)',
                    '-o-transform': 'translate(122px, -2px)',
                    'transform': 'translate(122px, -2px)'
                });

            }


        });
    },
    duration: 1000
}, 'easeOutCubic');


//bg animate layer a bit slower also seen on amountMovedX.

mock2.animate({
    textIndent: 0
}, {
    step: function (now, fx) {
        overlay.mousemove(function (e) {
            // var amountMovedX = (e.pageX * -1 / 10);
            var amountMovedY = (e.pageY * -0.5 / 6);
            // var amountMovedY = (e.pageY);    
            // alert(amountMovedY);

            if (amountMovedY <= (-18)) {
                mock2.css({
                    '-webkit-transform': 'translate(' + amountMovedY + 'px, 0)',
                    '-moz-transform': 'translate(' + amountMovedY + 'px, )',
                    '-ms-transform': 'translate(' + amountMovedY + 'px, 0)',
                    '-o-transform': 'translate(' + amountMovedY + 'px, 0)',
                    'transform': 'translate(' + amountMovedY + 'px, 0)'
                });
            } else {
                mock2.css({
                    '-webkit-transform': 'translate(56px, -2px)',
                    '-moz-transform': 'translate(56px, -2px)',
                    '-ms-transform': 'translate(56px, -2px)',
                    '-o-transform': 'translate(56px, -2px)',
                    'transform': 'translate(56px, -2px)'
                });
            }

        });
    },
    duration: 3000
}, 'easeOutCubic');


    var domesticBtn =  document.getElementById('domesticBtn');
    var internationalBtn =  document.getElementById('internationalBtn');
 
    var domesticProjects =  document.getElementById('domesticProjects');
    var internationalProjects =  document.getElementById('internationalProjects');

    function tabChange(tab) {
        if (tab == 1) {
            domesticBtn.classList.add('tab-btn-active');
            internationalBtn.classList.remove('tab-btn-active');

            domesticProjects.classList.add('tab-active');
            internationalProjects.classList.remove('tab-active');
        }else{
            internationalBtn.classList.add('tab-btn-active');
            domesticBtn.classList.remove('tab-btn-active');

            internationalProjects.classList.add('tab-active');
            domesticProjects.classList.remove('tab-active');
        }
    }